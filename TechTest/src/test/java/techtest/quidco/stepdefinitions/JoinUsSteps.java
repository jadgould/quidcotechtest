package techtest.quidco.stepdefinitions;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;

import techtest.quidco.screenobjects.homeSO;
import techtest.quidco.screenobjects.joinusSO;
import techtest.quidco.screenobjects.signInSO;
import techtest.quidco.screenobjects.splashSO;
import techtest.quidco.util.AppiumUtil;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class JoinUsSteps extends AppiumUtil {
	
	@Given("^the Join Us screen is being displayed$")
	public void join_us_screen_being_displayed() throws MalformedURLException
	{
		startApp();
		
		splashSO splash = new splashSO();
		splash.tap_next_button();
		splash.tap_next_button();
		splash.tap_next_button();
		
		signInSO signIn = new signInSO();
	    signIn.tap_sign_in_join_us_button();
	    
	    joinusSO joinus = new joinusSO();
	    assertTrue(joinus.exists());
	}
	
	@When("^valid details are entered$")
	public void valid_details_entered() throws MalformedURLException
	{
		joinusSO joinus = new joinusSO();
		joinus.enter_valid_details();
	}
	
	@And("^the Join Us button is tapped$")
	public void join_us_button_tap() throws MalformedURLException
	{
		joinusSO joinus = new joinusSO();
		joinus.tap_join_us_button();
	}
	
	@Then("^the Home screen will be displayed$")
	public void home_screen_being_displayed() throws MalformedURLException
	{
		homeSO home = new homeSO();
		home.exists();
	}
	
  @After
  public void closeAppDown(){
    if(driver != null && driver.getSessionId() != null)
    {
      driver.closeApp();
      driver.quit();
    }
  }
	
}
package techtest.quidco.stepdefinitions;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;

import techtest.quidco.screenobjects.splashSO;
import techtest.quidco.util.AppiumUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SplashSteps extends AppiumUtil {
	
	@Given("^The app is installed$")
	public void app_is_installed() throws MalformedURLException
	{

	}
	
	@When("^the app is opened$")
	public void start_app() throws MalformedURLException
	{
		startApp();
	}

	@Then("^the splash screen will be displayed$")
	public void splash_screen_displayed()
	{
		splashSO splash = new splashSO();
	    assertTrue(splash.exists());
	}
	
}

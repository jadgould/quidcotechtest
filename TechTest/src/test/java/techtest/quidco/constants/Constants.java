package techtest.quidco.constants;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class Constants {

	public final static String DEVICE_NAME = "Android Emulator";
	public final static String PLATFORM_VERSION = "6.0";
	public final static String PLATFORM_NAME = "Android";
	public final static String APP = "/Users/jamesgould/Documents/Bitbucket/workspace/QuidcoTechTest/TechTest/src/test/java/techtest/quidco/Quidco Cashback Discounts Voucher Codes_v3.3.4_apkpurecom.apk";
	public final static String AVD = "Nexus_5X_API_23";
	public final static String AUTOMATION_NAME = "Appium";
	
	public final static String JOINFIRSTNAME = "testfirstname";
	public final static String JOINLASTNAME = "testlastname";
	public final static String JOINEMAILNAME = "testemailaddress";
	public final static String JOINPASSWORD = "testpassword";
	
//	Splash screen
	
	@AndroidFindBy(id="next_button") public static AndroidElement nextbutton;

//	Sign In screen
	
	@AndroidFindBy(id="join_button") public static AndroidElement signinjoinusbutton;
	
//	Join US screen
	
	@AndroidFindBy(id="join_join_button") public static AndroidElement joinusbutton;
	
//	Home screen
	
	@FindBy(id="com.quidco:id/toolbar") public static AndroidElement homescreentitle;
	
}
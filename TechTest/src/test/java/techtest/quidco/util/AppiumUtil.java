package techtest.quidco.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import techtest.quidco.constants.Constants;

public class AppiumUtil extends Constants {

    public static AppiumDriver driver;
    public final static int IMPLICIT_TIME_SECONDS = 30;
    
    public void init()
    {
    	DesiredCapabilities caps = DesiredCapabilities.iphone();
    }
    
	public static void startApp() throws MalformedURLException {
		driver=new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), getDesiredCapabilitiesForAndroid());
	}

	private static Capabilities getDesiredCapabilitiesForAndroid() {
		
		DesiredCapabilities caps = DesiredCapabilities.android();
		caps.setCapability("deviceName", DEVICE_NAME);
		caps.setCapability("platformName", PLATFORM_NAME);
		caps.setCapability("AVD", AVD);
		caps.setCapability("app", APP);
		caps.setCapability("automationName", AUTOMATION_NAME);
		
		return caps;
	}

	public static Boolean checkElementDisplayed(AndroidElement element)
    {
    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        try 
        {
        	Boolean b = element.isDisplayed();
        	driver.manage().timeouts().implicitlyWait(IMPLICIT_TIME_SECONDS, TimeUnit.SECONDS);
        	return b;
        } 
        catch (Exception e) 
        {
        	driver.manage().timeouts().implicitlyWait(IMPLICIT_TIME_SECONDS, TimeUnit.SECONDS);
        	return false;
        }        
    }
	
	public static AndroidElement waitUntillTappable(AndroidElement element)
	{
		 WebDriverWait wait = new WebDriverWait(driver, 20);
		 wait.until(ExpectedConditions.elementToBeClickable(element));
		 return element;
	}

	public static WebElement waitUntillClickable(WebElement element)
	{
		 WebDriverWait wait = new WebDriverWait(driver, 20);
		 wait.until(ExpectedConditions.elementToBeClickable(element));
		 return element;
	}
	
	public static void tap_button(AndroidElement region) {
		waitUntillTappable(region);
		driver.tap(1, region, 1);
	}
	
	public static Boolean selectedState(AndroidElement region) {
		return region.isSelected();
	}

	public Boolean isObjectDisplayed(AndroidElement topic) {
		return topic.isDisplayed();		
	}
	
	public static void enter_first_name() {
		driver.findElement(By.id("com.quidco:id/join_first_name")).sendKeys("test");
	}
	
	public static void enter_last_name() {
		driver.findElement(By.id("com.quidco:id/join_last_name")).sendKeys("test");
	}
	
	public static void enter_email() {
		driver.findElement(By.id("com.quidco:id/join_email_address")).sendKeys("abcde@test.com");
	}
	
	public static void enter_password() {
		driver.findElement(By.id("com.quidco:id/join_password")).sendKeys("TestTest1");
		driver.hideKeyboard();
	}
	
}
package techtest.quidco.screenobjects;;

public class splashSO extends screenobject {
	
	public Boolean exists() {
		return checkElementDisplayed(nextbutton);
	}
	
	public void tap_next_button() {
		
		tap_button(nextbutton);
		
	}
	
}
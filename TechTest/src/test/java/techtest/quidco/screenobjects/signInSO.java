package techtest.quidco.screenobjects;;

public class signInSO extends screenobject {
	
	public Boolean exists() {
		return checkElementDisplayed(signinjoinusbutton);
	}

	public void tap_sign_in_join_us_button() {
		
		tap_button(signinjoinusbutton);
		
	}
	
}
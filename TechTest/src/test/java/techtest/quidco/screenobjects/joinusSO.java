package techtest.quidco.screenobjects;;

public class joinusSO extends screenobject {
	
	public Boolean exists() {
		return checkElementDisplayed(joinusbutton);
	}
	
	public void tap_join_us_button() {
		
		tap_button(joinusbutton);
		
	}

	public void enter_valid_details() {
		
		enter_first_name();
		enter_last_name();
		enter_email();
		enter_password();
		
	}
	
}